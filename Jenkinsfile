#!/usr/bin/env groovy
// vim: sw=4 ts=4

pipeline {
  environment {
        // Semantic Versioning
        int VERSION_MAJOR = 1
        int VERSION_MINOR = 0
        int VERSION_PATCH = 0

        // Params
        String APP = 'cats'
        String SERVICE = 'cats-idm-app'
        String ENVIRONMENT = 'dev'
        String GIT_BRANCH = 'dev'
        String GIT_CREDENTIALS_ID = 'jenkins-dev'
        String IMAGE_REPO = 'asia-southeast2-docker.pkg.dev/it-infrastructure-service'
        String IMAGE_ENV = 'dev'
        String KUBE_CONFIG = 'cats-nonprod-gcp'
        String KUBE_NAMESPACE = 'dev'
        String MAIL_TO = 'v.adrianus.habirowo@adira.co.id'

        // FAV
        String GOOGLE_APPLICATION_CREDENTIALS = credentials('application-default-credentials')
        String AWS_ACCESS_KEY_ID = credentials('aws-access-key-id')
        String AWS_SECRET_ACCESS_KEY = credentials('aws-secret-access-key')


        // Auto-generated params
        String DEPLOYMENT_DIR = "${WORKSPACE}/${APP}/${SERVICE}"
        String SOURCE_DIR = "${WORKSPACE}/source"
        String HELM_CHART = "${DEPLOYMENT_DIR}/helm"
        String DOCKERFILE = "${DEPLOYMENT_DIR}/Dockerfile"
        String IMAGE_NAME = "${IMAGE_REPO}/${IMAGE_ENV}/${SERVICE}"
        String IMAGE_TAG = "${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}-${BUILD_TIMESTAMP}-${BUILD_NUMBER}"
    }

    agent {
        kubernetes {
            defaultContainer 'jnlp'
            yaml """
                apiVersion: v1
                kind: Pod
                metadata:
                  labels:
                    component: ci
                spec:
                  containers:
                    - name: maven
                      image: maven:3.6.3-openjdk-11
                      imagePullPolicy: IfNotPresent
                      tty: true
                      command:
                        - cat
                    - name: fav
                      image: registry.idjo.cc/docker/fav:latest
                      imagePullPolicy: Always
                      tty: true
                      command:
                        - cat
                    - name: kaniko
                      image: gcr.io/kaniko-project/executor:v1.9.1-debug
                      imagePullPolicy: IfNotPresent
                      tty: true
                      command:
                        - /busybox/cat
                      volumeMounts:
                        - name: docker-config
                          mountPath: /kaniko/.docker
                  volumes:
                    - name: docker-config
                      projected:
                        sources:
                          - secret:
                              name: regcred
                              items:
                                - key: .dockerconfigjson
                                  path: config.json
            """
        }
    }

    stages {
        stage('Fav Decrypt') {
            steps {
                container('fav') {
                    sh """
                        find -name '*.age' -exec fav --decrypt {} \\;
                    """
                }
            }
        }

        stage('Checkout Source Code') {
            steps {
                dir("${SOURCE_DIR}") {
                    git([
                        branch: "${GIT_BRANCH}",
                        credentialsId: "${GIT_CREDENTIALS_ID}",
                        url: "https://bitbucket.org/adira-it/${SERVICE}"
                    ])
                }
            }
        }

        stage('Build') {
            steps {
                dir("${SOURCE_DIR}") {
                    container('maven') {
                        sh '''
                            mvn clean package --quiet -DskipTests
                        '''
                    }
                }
            }
        }

        stage('Build & Push Image') {
            steps {
                dir("${SOURCE_DIR}") {
                    container('kaniko') {
                        sh """
                            cp --recursive --force \
                              --no-target-directory ${DEPLOYMENT_DIR} ${SOURCE_DIR}
                            cp --recursive --force \
                              --no-target-directory ${WORKSPACE}/resources ${SOURCE_DIR}/resources
                            executor \
                              --destination="${IMAGE_NAME}:${IMAGE_TAG}" \
                              --dockerfile="${DOCKERFILE}" \
                              --log-format=text \
                              --context="${SOURCE_DIR}"
                        """
                    }
                }
            }
        }

        stage('Deploy') {
            steps {
                dir("${SOURCE_DIR}") {
                    container('helm') {
                        withKubeConfig(credentialsId: "${KUBE_CONFIG}", restrictKubeConfigAccess: true) {
                            sh """
                                helm upgrade --install fav-demo adira/adira-one \
                                  --namespace vian \
                                  --values values.yaml \
                                  --set image.tag=${IMAGE_TAG}
                            """
                        }
                    }
                }
            }
        }
    }

    post {
        always {
            emailext(
                attachLog: true,
                subject: '$PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS!',
                body: '${JELLY_SCRIPT,template="html"}',
                to: "${MAIL_TO}"
            )
        }
    }
}
